class VariableComparer:
    '''
    判断两个变量是否相等
    '''

    
    def __init__(self, variable1, variable2):
        self.variable1 = variable1
        self.variable2 = variable2

    def compare_variables(self):
        if type(self.variable1) != type(self.variable2):
            return False

        if self.variable1 == self.variable2:
            return True
        else:
            return False

# 示例用法
variable1 = 10
variable2 = 20
comparer = VariableComparer(variable1, variable2)
result = comparer.compare_variables()
print(result)  # 输出 False