#include <vector>

//遍历删除元素会导致core dump
int main()
{
  std::vector<int> vec= {1,2,3,4,5};
  for (auto itr = vec.begin(); itr != vec.end(); ++itr) {
    if (*itr == 5) {
      vec.erase(itr);
    }
  }
  return 0;
}
