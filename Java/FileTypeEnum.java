/*
微服务架构
file----文件服务
common----多个服务共用的文件，这里有个文件类型枚举FileTypeEnum.java
aaa
bbb
ccc ----业务服务
*/

public enum FileTypeEnum{
    XXX, 
    YYY,
    ZZZ
}

/*
   如果业务服务(如aaa,bbb,ccc)里需要增加文件类型，就得在common模块FileTypeEnum.java的枚举里增加一个枚举值。
   然后重新编译common，再重新构建部署file服务


   我怎么想也想不明白，这种频繁变动的东西，为什么会用枚举
*/